<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Au008 extends CI_Controller
{
    public $idsc;
    public $aksesc = array();
    function __construct()
    {
        parent::__construct();
        $idformini = "au008";
        $data["datalogin"] = $this->Mlogin->cek_login();
        $dataform = $this->Mlogin->cek_sistem($idformini);
        if (is_array($data["datalogin"])) {
            foreach ($dataform as $cx) {
                $idsistem_sistem = $cx->id_sistem;
            }
            $idsistem_user = array();
            foreach ($data["datalogin"] as $dl) {
                $idlevel = $dl->id_level;
                array_push($idsistem_user, $dl->id_sistem);
            }
            if (array_search($idsistem_sistem, $idsistem_user) !== false) {
            } else {
                redirect(base_url());
            }
            $data["datamenu"] = $this->Mlogin->cek_menu($idsistem_sistem, $idlevel);
            $data["dataform"] = $this->Mlogin->cek_form($idsistem_sistem, $idlevel);
            $data["ids"] = $idsistem_sistem;
            $data["idf"] = $idformini;
            $this->idsc = $idsistem_sistem;
            $idform = array();
            $akses = array();
            foreach ($data["dataform"] as $dx) {
                array_push($idform, $dx->id_form);
                if ($dx->id_form == $idformini) {
                    array_push($akses, $dx->akses_tambah, $dx->akses_update, $dx->akses_hapus, $dx->akses_cetak);
                }
            }
            if (array_search($idformini, $idform) !== false) {
                $data["akses"] = $akses;
                $this->aksesc = $akses;
            } else {
                redirect(base_url());
            }
            $this->load->view($idsistem_sistem . '/basis', $data, true);
        } else {
            redirect(base_url());
        }
    }

    public function index()
    {
        $data["fill"] = "au008v";
        $data["dtakses"] = $this->Mak755->data();
        $data["dtunit"] = $this->Mbp008->data();
        $this->load->view($this->idsc . '/basis', $data);
    }

    public function json()
    {
        $dtJSON = '{"data": [xxx]}';
        $dtisi = "";
        $dt = $this->Mau008->data();
        foreach ($dt as $k) {
            $id = $k->id;
            $id_akses = $k->akses;
            $id_unit = $k->baba;
            // $level = $k->level;
            // if($k->status == "Y"){$status = "Aktif";}else{$status = "Tak Aktif";}
            $tomboledit = "<button class='btn btn-icon btn-round btn-primary' data-id='" . $id . "' onclick='filter(this)'><i class='icon wb-pencil'></i></button>";
            $dtisi .= '["' . $tomboledit . '","' . $id . '","' . $id_akses . '","' . $id_unit . '"],';
        }
        $dtisifix = rtrim($dtisi, ",");
        $data = str_replace("xxx", $dtisifix, $dtJSON);
        echo $data;
    }

    public function filter()
    {
        $id = trim($this->input->post("id"));
        $dt = $this->Mau008->filter($id);
        if (is_array($dt)) {
            if (count($dt) > 0) {
                foreach ($dt as $k) {
                    $id = $k->id;
                    $id_akses = $k->id_akses;
                    $id_unit = $k->id_unit;
                    // $username = $k->username;
                    // $idlevel = $k->id_level;
                    // $status = $k->status;
                }
                echo base64_encode("1|" . $id . "|" . $id_akses . "|" . $id_unit);
            } else {
                echo base64_encode("0|");
            }
        } else {
            echo base64_encode("0|");
        }
    }

    public function tambah()
    {
        if ($this->aksesc[0] == "1") {
            $id_akses = trim(str_replace("'", "''", $this->input->post("id_akses")));
            $id_unit = trim(str_replace("'", "''", $this->input->post("id_unit")));
            // $level = trim(str_replace("'","''",$this->input->post("level")));
            // $status = trim(str_replace("'","''",$this->input->post("status")));
            // $pass =  md5(base64_encode(enkripsi($username)));
            // $dt = $this->Mau008->filterusername($username);
            // if(is_array($dt)){
            // 	if(count($dt) > 0){
            // 		echo base64_encode("80");
            // 		return;
            // 	}
            // }
            $operasi = $this->Mau008->tambah($id_akses, $id_unit);
            if ($operasi == "1") {
                $ket = "ID_akses: $id_akses,\nID_unit: $id_unit";
                $this->Mlog->log_history("008_akun", "Tambah", $ket);
            }
            echo base64_encode($operasi);
        } else {
            echo base64_encode("99");
        }
    }

    public function update()
    {
        if ($this->aksesc[1] == "1") {
            $id = trim(str_replace("'", "''", $this->input->post("id")));
            $id_akses = trim(str_replace("'", "''", $this->input->post("id_akses")));
            $id_unit = trim(str_replace("'", "''", $this->input->post("id_unit")));
            // $status = trim(str_replace("'", "''", $this->input->post("status")));
            $operasi = $this->Mau008->update($id, $id_akses, $id_unit);
            if ($operasi == "1") {
                $ket = "ID Akun: $id,\nID_akses: $id_akses,\nID_unit: $id_unit";
                $this->Mlog->log_history("008_akun", "Update", $ket);
            }
            echo base64_encode($operasi);
        } else {
            echo base64_encode("99");
        }
    }

    public function hapus()
    {
        if ($this->aksesc[2] == "1") {
            $id = trim(str_replace("'", "''", $this->input->post("id")));
            $td = $this->Mau008->cekform($id);
            if (is_array($td)) {
                if (count($td) > 0) {
                    echo base64_encode("90");
                } else {
                    $dt = $this->Mau008->filter($id);
                    $operasi = $this->Mau008->hapus($id);
                    if ($operasi == "1") {
                        foreach ($dt as $k) {
                            $id = $k->id;
                            $id_akses = $k->id_akses;
                            $id_unit = $k->id_unit;
                            // $idlevel = $k->id_level;
                            // $status = $k->status;
                        }
                        $ket = "ID Akun: $id,\nID_akses: $id_akses,\nID_unit: $id_unit";
                        $this->Mlog->log_history("008_akun", "Hapus", $ket);
                    }
                    echo base64_encode($operasi);
                }
            } else {
                echo base64_encode("80");
            }
        } else {
            echo base64_encode("99");
        }
    }

    // public function reset()
    // {
    //     $id = trim(str_replace("'", "''", $this->input->post("id")));
    //     $td = $this->Mau008->filter($id);
    //     if (is_array($td)) {
    //         if (count($td) > 0) {
    //             foreach ($td as $k) {
    //                 $id = $k->id;
    //                 $nama = $k->nama;
    //                 $username = $k->username;
    //             }
    //             $pass =  md5(base64_encode(enkripsi($username)));
    //             $operasi = $this->Mau008->reset($id, $pass);
    //             if ($operasi == "1") {
    //                 $ket = "ID Akun: $id,\nNama Akun: $nama,\nUsername: $username";
    //                 $this->Mlog->log_history("008_akun", "Reset Password", $ket);
    //             }
    //             echo base64_encode($operasi);
    //         } else {
    //             echo base64_encode("90");
    //         }
    //     } else {
    //         echo base64_encode("80");
    //     }
    // }
}
