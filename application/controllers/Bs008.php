<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Bs008 extends CI_Controller {
	public $idsc;
	public $aksesc = array();
	function __construct() {
		parent::__construct();
		$this->load->helper(['url','form','an_fungsi']);
		$idformini = "bs008";
		$data["datalogin"] = $this->Mlogin->cek_login();
		$dataform = $this->Mlogin->cek_sistem($idformini);
        if(is_array($data["datalogin"])){
			foreach($dataform as $cx){
				$idsistem_sistem = $cx->id_sistem;
			}
			$idsistem_user = array();
         	foreach ($data["datalogin"] as $dl){
				$idlevel = $dl->id_level;
				array_push($idsistem_user, $dl->id_sistem);
			}
			if(array_search($idsistem_sistem, $idsistem_user) !== false){}else{redirect(base_url());}
			$data["datamenu"] = $this->Mlogin->cek_menu($idsistem_sistem, $idlevel);
			$data["dataform"] = $this->Mlogin->cek_form($idsistem_sistem, $idlevel);
			$data["ids"] = $idsistem_sistem;
			$data["idf"] = $idformini;
			$this->idsc = $idsistem_sistem;
			$idform = array(); $akses = array();
			foreach ($data["dataform"] as $dx){
				array_push($idform, $dx->id_form);
				if($dx->id_form == $idformini){array_push($akses, $dx->akses_tambah, $dx->akses_update, $dx->akses_hapus, $dx->akses_cetak);}
			}
			if(array_search($idformini, $idform) !== false){
				$data["akses"] = $akses;
				$this->aksesc = $akses; 
			}else{redirect(base_url());}
        	$this->load->view($idsistem_sistem.'/basis', $data, true);
        }else{redirect(base_url());}
    }

	public function index(){
		$fill = "bs008v";
        $title = "Buku Besar";
		$listJurnal = $this->Mbs008->list_jurnal();
		
		$this->load->view($this->idsc.'/basis', compact('fill','title','listJurnal'));
	}

	public function detail()
	{
		$bulan = $this->input->post('bulan',true);
        $tahun = $this->input->post('tahun',true);
		
		$fill = "bs008v";
        $title = "Buku Besar $bulan/$tahun";
		$dataAkun = $this->Mbs008->data($bulan, $tahun);
        $data_akun = null;
        $saldo = null;
        foreach($dataAkun as $row){
            $data_akun[] = (array) $this->Mbs008->get_nama($row->nama, $bulan, $tahun);
            $saldo[] = (array) $this->Mbs008->get_saldo($row->nama, $bulan, $tahun);
        }
        $jumlah = count($data_akun);
        $this->load->view($this->idsc.'/basis', compact('fill','title','dataAkun','data_akun','saldo','jumlah'));
	}
}
