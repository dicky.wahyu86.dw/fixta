<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jurnal extends CI_Controller {
	public $idsc;
	public $aksesc = array();
	function __construct()
	{
		parent::__construct();
		$idformini = "tr008";
		$data["datalogin"] = $this->Mlogin->cek_login();
		$dataform = $this->Mlogin->cek_sistem($idformini);
		if (is_array($data["datalogin"])) {
			foreach ($dataform as $cx) {
				$idsistem_sistem = $cx->id_sistem;
			}
			$idsistem_user = array();
			foreach ($data["datalogin"] as $dl) {
				$idlevel = $dl->id_level;
				array_push($idsistem_user, $dl->id_sistem);
			}
			if (array_search($idsistem_sistem, $idsistem_user) !== false) {
			} else {
				redirect(base_url());
			}
			$data["datamenu"] = $this->Mlogin->cek_menu($idsistem_sistem, $idlevel);
			$data["dataform"] = $this->Mlogin->cek_form($idsistem_sistem, $idlevel);
			$data["ids"] = $idsistem_sistem;
			$data["idf"] = $idformini;
			$this->idsc = $idsistem_sistem;
			$idform = array();
			$akses = array();
			foreach ($data["dataform"] as $dx) {
				array_push($idform, $dx->id_form);
				if ($dx->id_form == $idformini) {
					array_push($akses, $dx->akses_tambah, $dx->akses_update, $dx->akses_hapus, $dx->akses_cetak);
				}
			}
			if (array_search($idformini, $idform) !== false) {
				$data["akses"] = $akses;
				$this->aksesc = $akses;
			} else {
				redirect(base_url());
			}
			$this->load->view($idsistem_sistem . '/basis', $data, true);
		} else {
			redirect(base_url());
		}
	}

	public function index()
	{
		$data["fill"] = "tr008v";
		$data["dtakun"] = $this->Mda008->data();
		$data["transaksi"] = $this->Mtr008->data();
		$data['total_debet'] = $this->db->query("SELECT SUM(008_transaksi1.nominal) AS debet FROM 008_transaksi1 INNER JOIN 008_akuntansi1 ON 008_transaksi1.id_akun=008_akuntansi1.id WHERE 008_akuntansi1.status='debit' AND 008_transaksi1.status_data='aktif'")->row();

		$data['total_kredit'] = $this->db->query("SELECT SUM(008_transaksi1.nominal) AS kredit FROM 008_transaksi1 INNER JOIN 008_akuntansi1 ON 008_transaksi1.id_akun=008_akuntansi1.id WHERE 008_akuntansi1.status='kredit' AND 008_transaksi1.status_data='aktif'")->row();
		$this->load->view($this->idsc . '/basis', $data);
	}
}
