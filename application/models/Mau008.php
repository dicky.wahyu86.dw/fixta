<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Mau008 extends CI_Model
{
    public function data()
    {
        $sql = "SELECT a.*, b.nama AS akses, c.nama AS baba FROM 008_akun AS a LEFT JOIN akses AS b ON a.id_akses = b.id LEFT JOIN 008_kantor AS c ON a.id_unit = c.id ORDER BY id";
        $querySQL = $this->db->query($sql);
        if ($querySQL) {
            return $querySQL->result();
        } else {
            return 0;
        }
    }

    public function filter($a)
    {
        $sql = "SELECT * FROM 008_akun WHERE id='$a' ORDER BY id";
        $querySQL = $this->db->query($sql);
        if ($querySQL) {
            return $querySQL->result();
        } else {
            return 0;
        }
    }

    public function filterusername($a)
    {
        $sql = "SELECT * FROM 008_akun WHERE username='$a' ORDER BY id";
        $querySQL = $this->db->query($sql);
        if ($querySQL) {
            return $querySQL->result();
        } else {
            return 0;
        }
    }

    public function cekform($a)
    {
        $sql = "SELECT * FROM log_history WHERE id_user='$a' ORDER BY id";
        $querySQL = $this->db->query($sql);
        if ($querySQL) {
            return $querySQL->result();
        } else {
            return 0;
        }
    }

    public function tambah($id_akses, $id_unit)
    {
        $user = $this->Mlogin->ambiluser();
        $sql = "INSERT INTO 008_akun VALUES(UNIX_TIMESTAMP(NOW()),'$id_akses','$id_unit',NOW(),'0000-00-00','$user','');";
        $querySQL = $this->db->query($sql);
        if ($querySQL) {
            return "1";
        } else {
            return "0";
        }
    }

    public function update($id, $id_akses, $id_unit)
    {
        $user = $this->Mlogin->ambiluser();
        $sql = "UPDATE 008_akun SET id_akses='$id_akses', id_unit='$id_unit', tgl_update=NOW(), id_update='$user' WHERE id='$id';";
        $querySQL = $this->db->query($sql);
        if ($querySQL) {
            return "1";
        } else {
            return "0";
        }
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM 008_akun WHERE id='$id';";
        $querySQL = $this->db->query($sql);
        if ($querySQL) {
            return "1";
        } else {
            return "0";
        }
    }

    // public function reset($id, $pass)
    // {
    //     $user = $this->Mlogin->ambiluser();
    //     $sql = "UPDATE 008_akun SET password='$pass', tgl_update=NOW(), id_update='$user' WHERE id='$id';";
    //     $querySQL = $this->db->query($sql);
    //     if ($querySQL) {
    //         return "1";
    //     } else {
    //         return "0";
    //     }
    // }
}
