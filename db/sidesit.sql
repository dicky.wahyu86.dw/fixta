-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Jan 2023 pada 20.00
-- Versi server: 10.4.25-MariaDB
-- Versi PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sidesit`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `008_akun`
--

CREATE TABLE `008_akun` (
  `id` char(25) CHARACTER SET latin1 NOT NULL,
  `id_akses` char(25) CHARACTER SET latin1 NOT NULL,
  `id_unit` char(25) CHARACTER SET latin1 NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) CHARACTER SET latin1 NOT NULL,
  `id_update` char(25) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `008_akun`
--

INSERT INTO `008_akun` (`id`, `id_akses`, `id_unit`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('1655376606', '1655363289', '000', '2022-06-16 17:50:06', '2022-07-26 14:19:53', '1586930688', '1586930688'),
('1658820016', '1658819612', '001', '2022-07-26 14:20:16', '0000-00-00 00:00:00', '1586930688', ''),
('1658820031', '1658819647', '002', '2022-07-26 14:20:31', '0000-00-00 00:00:00', '1586930688', ''),
('1658820046', '1658819693', '003', '2022-07-26 14:20:46', '0000-00-00 00:00:00', '1586930688', ''),
('1658820061', '1658819746', '004', '2022-07-26 14:21:01', '0000-00-00 00:00:00', '1586930688', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `008_akuntansi1`
--

CREATE TABLE `008_akuntansi1` (
  `id` char(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `status` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `008_akuntansi1`
--

INSERT INTO `008_akuntansi1` (`id`, `nama`, `keterangan`, `id_buat`, `id_update`, `tgl_buat`, `tgl_update`, `status`) VALUES
('11201', 'Piutang', 'Piutang Badan Usaha', '1586930688', '1586930688', '2021-06-27 14:01:21', '2021-06-27 19:27:13', 'debit'),
('11101', 'Kas', 'Kas', '1586930688', '1586930688', '2021-06-27 14:28:36', '2021-08-26 23:21:55', 'debit'),
('11301', 'Sewa Dibayar Dimuka', 'Sewa Dibayar Dimuka', '1586930688', '1586930688', '2021-06-27 16:53:04', '2021-08-26 23:44:30', 'debit'),
('12101', 'Peralatan', 'Peralatan Badan Usaha', '1586930688', '1586930688', '2021-06-27 19:22:54', '2021-08-26 23:43:51', 'debit'),
('12201', 'Akumulasi Penyusutan Peralatan', 'Akumulasi Penyusutan Peralatan Badan Usaha', '1586930688', '1586930688', '2021-06-27 19:23:35', '2021-08-26 23:43:41', 'debit'),
('31101', 'Modal', 'Modal', '1586930688', '1586930688', '2021-06-27 19:24:48', '2021-08-26 23:40:13', 'debit'),
('11401', 'Iklan Dibayar Dimuka', 'Iklan Dibayar Dimuka', '1586930688', '1586930688', '2021-06-27 19:30:33', '2021-08-26 23:44:21', 'debit'),
('11501', 'Perlengkapan', 'Perlengkapan Badan Usaha', '1586930688', '1586930688', '2021-06-27 19:31:09', '2021-08-26 23:44:11', 'debit'),
('11601', 'Asuransi Dibayar Dimuka', 'Asuransi Dibayar Dimuka', '1586930688', '1586930688', '2021-06-27 19:31:44', '2021-08-26 23:44:01', 'debit'),
('12301', 'Kendaraan', 'Kendaraan Badan Usaha', '1586930688', '1586930688', '2021-06-27 19:35:15', '2021-08-26 23:43:34', 'debit'),
('12401', 'Akumulasi Kendaraan', 'Akumulasi Penyusutan Kendaraan Badan Usaha', '1586930688', '1586930688', '2021-06-27 19:36:39', '2021-08-26 23:43:28', 'debit'),
('21101', 'Utang Usaha', 'Utang Usaha', '1586930688', '1586930688', '2021-06-27 19:39:37', '2021-08-26 23:43:19', 'debit'),
('21201', 'Utang BANK', 'Utang BANK', '1586930688', '1586930688', '2021-06-27 19:40:27', '2021-08-26 23:40:31', 'debit'),
('21301', 'Utang Bunga', 'Utang Bunga', '1586930688', '1586930688', '2021-06-27 19:40:55', '2021-08-26 23:40:23', 'debit'),
('31201', 'Prive', 'Prive Pengambilan Pribadi', '1586930688', '1586930688', '2021-06-27 19:43:04', '2021-08-26 23:40:05', 'debit'),
('41101', 'Pendapatan', 'Pendapatan', '1586930688', '1586930688', '2021-06-27 19:44:55', '2021-08-26 23:40:00', 'debit'),
('51101', 'Beban Listrik', 'Beban Listrik', '1586930688', '1586930688', '2021-06-27 19:49:42', '2021-08-26 23:39:55', 'debit'),
('51201', 'Beban Asuransi', 'Beban Asuransi', '1586930688', '1586930688', '2021-06-27 19:50:40', '2021-08-26 23:39:49', 'debit'),
('51301', 'Beban Gaji', 'Beban Gaji', '1586930688', '1586930688', '2021-06-27 19:51:45', '2021-08-26 23:07:04', 'debit'),
('11102', 'Kas', 'Kas Kredit', '1586930688', '', '2021-08-26 23:21:39', '0000-00-00 00:00:00', 'kredit'),
('11202', 'Piutang', 'Piutang Badan Usaha kredit', '1586930688', '1586930688', '2021-06-27 14:01:21', '2021-06-27 19:27:13', 'kredit'),
('11302', 'Sewa Dibayar Dimuka', 'Sewa Dibayar Dimuka kredit', '1586930688', '1586930688', '2021-06-27 16:53:04', '2021-06-27 19:29:49', 'kredit'),
('11402', 'Iklan Dibayar Dimuka', 'Iklan Dibayar Dimuka kredit', '1586930688', '1586930688', '2021-06-27 19:30:33', '2021-07-08 15:27:00', 'kredit'),
('11502', 'Perlengkapan', 'Perlengkapan Badan Usaha kredit', '1586930688', '', '2021-06-27 19:31:09', '0000-00-00 00:00:00', 'kredit'),
('11602', 'Asuransi Dibayar Dimuka', 'Asuransi Dibayar Dimuka kredit', '1586930688', '', '2021-06-27 19:31:44', '0000-00-00 00:00:00', 'kredit'),
('12102', 'Peralatan', 'Peralatan Badan Usaha kredit', '1586930688', '', '2021-06-27 19:22:54', '0000-00-00 00:00:00', 'kredit'),
('12202', 'Akumulasi Penyusutan Peralatan', 'Akumulasi Penyusutan Peralatan Badan Usaha kredit', '1586930688', '1586930688', '2021-06-27 19:23:35', '2021-06-27 19:33:42', 'kredit'),
('12302', 'Kendaraan', 'Kendaraan Badan Usaha kredit', '1586930688', '1586930688', '2021-06-27 19:35:15', '2021-06-27 19:35:49', 'kredit'),
('12402', 'Akumulasi Kendaraan', 'Akumulasi Penyusutan Kendaraan Badan Usaha kredit', '1586930688', '', '2021-06-27 19:36:39', '0000-00-00 00:00:00', 'kredit'),
('21102', 'Utang Usaha', 'Utang Usaha kredit', '1586930688', '', '2021-06-27 19:39:37', '0000-00-00 00:00:00', 'kredit'),
('21202', 'Utang BANK', 'Utang BANK kredit', '1586930688', '1586930688', '2021-06-27 19:40:27', '2021-06-27 19:41:20', 'kredit'),
('21302', 'Utang Bunga', 'Utang Bunga kredit', '1586930688', '1586930688', '2021-06-27 19:40:55', '2021-06-27 19:41:33', 'kredit'),
('31102', 'Modal', 'Modal kredit', '1586930688', '1586930688', '2021-06-27 19:24:48', '2021-06-27 19:43:51', 'kredit'),
('31202', 'Prive', 'Prive Pengambilan Pribadi kredit', '1586930688', '', '2021-06-27 19:43:04', '0000-00-00 00:00:00', 'kredit'),
('41102', 'Pendapatan', 'Pendapatan kredit', '1586930688', '', '2021-06-27 19:44:55', '0000-00-00 00:00:00', 'kredit'),
('51102', 'Beban Listrik', 'Beban Listrik kredit', '1586930688', '', '2021-06-27 19:49:42', '0000-00-00 00:00:00', 'kredit'),
('51202', 'Beban Asuransi', 'Beban Asuransi kredit', '1586930688', '', '2021-06-27 19:50:40', '0000-00-00 00:00:00', 'kredit'),
('51302', 'Beban Gaji', 'Beban Gaji kredit', '1586930688', '1586930688', '2021-06-27 19:51:45', '2021-08-26 23:07:04', 'kredit'),
('33333', 'aa', 'aaaa', '1586930688', '', '2021-08-27 04:19:12', '0000-00-00 00:00:00', 'debit'),
('10000', 'aaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaa', '1586930688', '', '2022-07-26 14:29:13', '0000-00-00 00:00:00', 'debit');

-- --------------------------------------------------------

--
-- Struktur dari tabel `008_kantor`
--

CREATE TABLE `008_kantor` (
  `id` char(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `008_kantor`
--

INSERT INTO `008_kantor` (`id`, `nama`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('001', 'Kolam Renang', '2021-03-16 16:50:43', '2022-06-16 13:27:17', '1586930688', '1586930688'),
('002', 'Kolam Pancing', '2022-06-16 13:39:52', '0000-00-00 00:00:00', '1586930688', ''),
('003', 'Jasa Foto Copy', '2022-06-16 13:41:21', '0000-00-00 00:00:00', '1586930688', ''),
('004', 'Jasa Hostpot Internet', '2022-06-16 13:42:04', '0000-00-00 00:00:00', '1586930688', ''),
('000', 'Bumdes PUSAT', '2022-07-26 14:19:39', '0000-00-00 00:00:00', '1586930688', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `008_transaksi1`
--

CREATE TABLE `008_transaksi1` (
  `id` char(25) NOT NULL,
  `id_akun` int(11) NOT NULL,
  `nominal` int(25) NOT NULL,
  `keterangan` varchar(200) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL,
  `status_data` varchar(50) NOT NULL,
  `status` varchar(100) NOT NULL,
  `bukti` varchar(225) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `008_transaksi1`
--

INSERT INTO `008_transaksi1` (`id`, `id_akun`, `nominal`, `keterangan`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`, `status_data`, `status`, `bukti`) VALUES
('1658123655', 11102, 1250000, 'Beli Hosting dan Domain', '2022-07-18 12:54:15', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'kredit', NULL),
('1656737484', 31101, 3000000, 'Modal Awal', '2022-07-02 11:51:24', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'debit', NULL),
('1670074386', 11102, 21000, 'Rokok Surya', '2022-12-03 20:33:06', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'kredit', NULL),
('1670078590', 11101, 200000, 'JP kakek MERAH', '2022-12-03 21:43:10', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'debit', NULL),
('1670079440', 12101, 13000, 'Minyak Goreng', '2022-12-03 21:57:20', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'debit', NULL),
('1673092907', 51102, 100000, 'Pulsa Token Listrik', '2023-01-07 19:01:47', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'kredit', NULL),
('1673092951', 12101, 10000, 'Chip 10K', '2023-01-07 19:02:31', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'debit', NULL),
('1673106711', 11201, 20000, 'Masku', '2023-01-07 22:51:51', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'debit', '8cfcb56b85aed3c4cfaca5ac21b3f538.png'),
('1673108079', 11202, 30000, 'Dinanyan', '2023-01-07 23:14:39', '0000-00-00 00:00:00', '1586930688', '', 'aktif', 'kredit', '1ba1cf1a530dd26c0a1ad85cc87a6074.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `akses`
--

CREATE TABLE `akses` (
  `id` char(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` text NOT NULL,
  `id_level` char(25) NOT NULL,
  `status` char(2) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `akses`
--

INSERT INTO `akses` (`id`, `nama`, `username`, `password`, `id_level`, `status`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('1586930688', 'Hero', 'super', '2989bc7f33155af0e3bfaf2f64a8ee9d', '00', 'Y', '2020-04-15 15:39:27', '2021-02-20 07:14:31', '', '1586930688'),
('1629992959', 'kulo', 'kulo', 'ae7a9888bc37e74d60b7ea442ec65a88', '01', 'Y', '2021-08-26 22:49:19', '2021-08-27 04:23:35', '1586930688', '1586930688'),
('1655363289', 'Admin BUMDes', 'dicky', '6a3eb04f68ea50990cef0d4ca243cd32', '01', 'Y', '2022-06-16 14:08:09', '2022-07-26 14:16:44', '1586930688', '1586930688'),
('1658819612', 'Admin Unit Kolam Renang', 'aukr001', '8e966b655bd6eaaf74c0177556c30345', '03', 'Y', '2022-07-26 14:13:32', '0000-00-00 00:00:00', '1586930688', ''),
('1658819647', 'Admin Unit Kolam Pancing', 'aukp002', 'c9d2aa9d8b46694ed9dac7be46fa0b41', '03', 'Y', '2022-07-26 14:14:07', '0000-00-00 00:00:00', '1586930688', ''),
('1658819693', 'Admin Unit Jasa Foto Copy', 'aujfc003', '3f3e63bff99490041551df2abc30b109', '03', 'Y', '2022-07-26 14:14:53', '0000-00-00 00:00:00', '1586930688', ''),
('1658819746', 'Admin Unit Jasa Hostpot Internet', 'aujhi004', '1b83d014a4dbab72bcbb2f8f536aa918', '03', 'Y', '2022-07-26 14:15:46', '0000-00-00 00:00:00', '1586930688', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `form`
--

CREATE TABLE `form` (
  `id` char(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `id_menu` char(25) NOT NULL,
  `id_sistem` char(25) NOT NULL,
  `icon` varchar(25) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `form`
--

INSERT INTO `form` (`id`, `nama`, `id_menu`, `id_sistem`, `icon`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('si933', 'Sistem', '00005', '001', 'wb-desktop', '2021-01-01 10:00:00', '2021-01-01 10:00:00', '', ''),
('me776', 'Menu', '00005', '001', 'wb-menu', '2021-01-01 10:00:00', '2021-01-01 10:00:00', '', ''),
('fo110', 'Form', '00005', '001', 'wb-contract', '2021-01-01 10:00:00', '2021-01-01 10:00:00', '', ''),
('le409', 'Level', '00005', '001', 'wb-flag', '2021-01-01 10:00:00', '2021-01-01 10:00:00', '', ''),
('ak755', 'Akun Akses', '00006', '001', 'wb-heart', '2021-02-20 06:29:58', '0000-00-00 00:00:00', '1586930688', ''),
('dr699', 'Form Level', '00006', '001', 'wb-cloud', '2021-02-20 09:30:40', '0000-00-00 00:00:00', '1586930688', ''),
('bp008', 'Data Unit', '00007', '008', 'wb-align-center', '2021-03-16 16:31:43', '2022-05-25 20:07:47', '1586930688', '1586930688'),
('da008', 'Master Jurnal', '00007', '008', 'wb-cloud', '2021-03-16 16:57:35', '2022-05-25 20:03:49', '1586930688', '1586930688'),
('tr008', 'Jurnal', '00008', '008', 'wb-bookmark', '2021-04-26 16:00:13', '2022-05-25 21:29:29', '1586930688', '1586930688'),
('ns008', 'Neraca', '00008', '008', 'wb-star-outline', '2022-05-25 21:31:29', '0000-00-00 00:00:00', '1586930688', ''),
('lr008', 'Laba Rugi', '00008', '008', 'wb-memory', '2022-05-25 21:32:44', '0000-00-00 00:00:00', '1586930688', ''),
('bs008', 'Buku Besar', '00008', '008', 'wb-desktop', '2022-05-25 21:33:51', '2022-05-25 21:34:42', '1586930688', '1586930688'),
('lp008', 'Laporan', '00008', '008', 'wb-clipboard', '2022-05-25 21:35:25', '0000-00-00 00:00:00', '1586930688', ''),
('tm008', 'Transaksi Masuk', '00009', '008', 'wb-plus', '2022-06-16 14:04:50', '0000-00-00 00:00:00', '1586930688', ''),
('tk008', 'Transaksi Keluar', '00009', '008', 'wb-minus', '2022-06-16 14:05:16', '0000-00-00 00:00:00', '1586930688', ''),
('au008', 'Tambah Akun Unit', '00007', '008', 'wb-users', '2022-06-16 14:38:26', '2022-06-16 14:48:59', '1586930688', '1586930688');

-- --------------------------------------------------------

--
-- Struktur dari tabel `form_level`
--

CREATE TABLE `form_level` (
  `id` char(25) NOT NULL,
  `id_level` char(25) NOT NULL,
  `id_form` char(25) NOT NULL,
  `akses_tambah` int(2) NOT NULL,
  `akses_update` int(2) NOT NULL,
  `akses_hapus` int(2) NOT NULL,
  `akses_cetak` int(2) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `form_level`
--

INSERT INTO `form_level` (`id`, `id_level`, `id_form`, `akses_tambah`, `akses_update`, `akses_hapus`, `akses_cetak`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('1655381531', '03', 'tr008', 0, 0, 0, 0, '2022-06-16 19:12:11', '2022-07-04 01:29:28', '1586930688', '1586930688'),
('1670075137', '01', 'bp008', 1, 1, 1, 1, '2022-12-03 20:45:37', '0000-00-00 00:00:00', '1586930688', ''),
('1670075153', '01', 'au008', 1, 1, 1, 1, '2022-12-03 20:45:53', '0000-00-00 00:00:00', '1586930688', ''),
('1670075176', '01', 'bs008', 1, 1, 1, 1, '2022-12-03 20:46:16', '0000-00-00 00:00:00', '1586930688', ''),
('1670075190', '01', 'da008', 1, 1, 1, 1, '2022-12-03 20:46:30', '0000-00-00 00:00:00', '1586930688', ''),
('1670075205', '01', 'lp008', 1, 1, 1, 1, '2022-12-03 20:46:45', '0000-00-00 00:00:00', '1586930688', ''),
('1670075221', '01', 'lr008', 1, 1, 1, 1, '2022-12-03 20:47:01', '0000-00-00 00:00:00', '1586930688', ''),
('1670075233', '01', 'ns008', 1, 1, 1, 1, '2022-12-03 20:47:13', '0000-00-00 00:00:00', '1586930688', ''),
('1670075243', '01', 'tk008', 1, 1, 1, 1, '2022-12-03 20:47:23', '0000-00-00 00:00:00', '1586930688', ''),
('1670075251', '01', 'tm008', 1, 1, 1, 1, '2022-12-03 20:47:31', '0000-00-00 00:00:00', '1586930688', ''),
('1670075263', '01', 'tr008', 1, 1, 1, 1, '2022-12-03 20:47:43', '0000-00-00 00:00:00', '1586930688', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `icon`
--

CREATE TABLE `icon` (
  `nama` varchar(100) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `icon`
--

INSERT INTO `icon` (`nama`) VALUES
('wb-add-file'),
('wb-alert'),
('wb-alert-circle'),
('wb-align-center'),
('wb-align-justify'),
('wb-align-left'),
('wb-align-right'),
('wb-arrow-down'),
('wb-arrow-expand'),
('wb-arrow-left'),
('wb-arrow-right'),
('wb-arrow-shrink'),
('wb-arrow-up'),
('wb-attach-file'),
('wb-bell'),
('wb-bold'),
('wb-book'),
('wb-bookmark'),
('wb-briefcase'),
('wb-calendar'),
('wb-camera'),
('wb-chat'),
('wb-chat-text'),
('wb-chat-working'),
('wb-chatgoup'),
('wb-check'),
('wb-check-circle'),
('wb-check-mini'),
('wb-chevron-down'),
('wb-chevron-down-mini'),
('wb-chevron-left'),
('wb-chevron-left-mini'),
('wb-chevron-right'),
('wb-chevron-right-mini'),
('wb-chevron-up'),
('wb-chevron-up-mini'),
('wb-clipboard'),
('wb-close'),
('wb-close-mini'),
('wb-cloud'),
('wb-code'),
('wb-code-unfold'),
('wb-code-working'),
('wb-contract'),
('wb-copy'),
('wb-crop'),
('wb-dashboard'),
('wb-desktop'),
('wb-download'),
('wb-dropdown'),
('wb-dropleft'),
('wb-dropright'),
('wb-dropup'),
('wb-edit'),
('wb-emoticon'),
('wb-envelope'),
('wb-envelope-open'),
('wb-expand'),
('wb-extension'),
('wb-eye'),
('wb-eye-close'),
('wb-file'),
('wb-flag'),
('wb-folder'),
('wb-format-clear'),
('wb-fullscreen'),
('wb-fullscreen-exit'),
('wb-gallery'),
('wb-globe'),
('wb-graph-down'),
('wb-graph-up'),
('wb-grid-4'),
('wb-grid-9'),
('wb-hammer'),
('wb-heart'),
('wb-heart-outline'),
('wb-help'),
('wb-help-circle'),
('wb-home'),
('wb-image'),
('wb-inbox'),
('wb-indent-decrease'),
('wb-indent-increase'),
('wb-info'),
('wb-italic'),
('wb-large-point'),
('wb-layout'),
('wb-library'),
('wb-link'),
('wb-link-broken'),
('wb-link-intact'),
('wb-list'),
('wb-list-bullet'),
('wb-list-numbered'),
('wb-lock'),
('wb-loop'),
('wb-map'),
('wb-medium-point'),
('wb-memory'),
('wb-menu'),
('wb-minus'),
('wb-minus-circle'),
('wb-mobile'),
('wb-more-horizontal'),
('wb-more-vertical'),
('wb-move'),
('wb-musical'),
('wb-order'),
('wb-paperclip'),
('wb-pause'),
('wb-payment'),
('wb-pencil'),
('wb-pie-chart'),
('wb-play'),
('wb-plugin'),
('wb-plus'),
('wb-plus-circle'),
('wb-pluse'),
('wb-power'),
('wb-print'),
('wb-quote-right'),
('wb-random'),
('wb-refresh'),
('wb-reload'),
('wb-replay'),
('wb-reply'),
('wb-rubber'),
('wb-scissor'),
('wb-search'),
('wb-settings'),
('wb-share'),
('wb-shopping-cart'),
('wb-signal'),
('wb-small-point'),
('wb-sort-asc'),
('wb-sort-des'),
('wb-sort-vertica;'),
('wb-star'),
('wb-star-half'),
('wb-star-outline'),
('wb-stop'),
('wb-table'),
('wb-tag'),
('wb-text'),
('wb-text-type'),
('wb-thumb-down'),
('wb-thumb-up'),
('wb-time'),
('wb-trash'),
('wb-triangle-down'),
('wb-triangle-left'),
('wb-triangle-right'),
('wb-triangle-up'),
('wb-underline'),
('wb-unlock'),
('wb-upload'),
('wb-user'),
('wb-user-add'),
('wb-user-circle'),
('wb-users'),
('wb-video'),
('wb-volume-high'),
('wb-volume-low'),
('wb-volume-off'),
('wb-warning'),
('wb-wrench'),
('wb-zoom-in'),
('wb-zoom-out');

-- --------------------------------------------------------

--
-- Struktur dari tabel `level`
--

CREATE TABLE `level` (
  `id` char(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `level`
--

INSERT INTO `level` (`id`, `nama`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('00', 'Super Admin', '2020-04-15 15:39:27', '0000-00-00 00:00:00', '', ''),
('01', 'Administrator', '2020-04-15 15:39:27', '2021-02-18 11:46:58', '', '1586930688'),
('03', 'Staf', '2021-02-18 12:13:36', '0000-00-00 00:00:00', '1586930688', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `log_history`
--

CREATE TABLE `log_history` (
  `id` char(25) NOT NULL,
  `data` varchar(50) NOT NULL,
  `operasi` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `id_user` char(25) NOT NULL,
  `tgl` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `log_history`
--

INSERT INTO `log_history` (`id`, `data`, `operasi`, `keterangan`, `id_user`, `tgl`) VALUES
('1630061344284183', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2021-08-27 17:49:04'),
('1653369316165952', 'Akses', 'Login', 'Login Berhasil', '1629992959', '2022-05-24 12:15:16'),
('1653369330325501', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-24 12:15:30'),
('1653480509037749', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-25 19:08:29'),
('1653483557109070', 'Form', 'Update', 'ID Form: bp008,\nNama Form: Unit Bumdes,\nID Menu: 00007,\nID Sistem: 008,\nIcon Form: wb-align-center', '1586930688', '2022-05-25 19:59:17'),
('1653483696790583', 'Form', 'Update', 'ID Form: tr008,\nNama Form: Laporan Bumdes,\nID Menu: 00007,\nID Sistem: 008,\nIcon Form: wb-random', '1586930688', '2022-05-25 20:01:36'),
('1653483771099404', 'Menu', 'Update', 'ID Menu: 00001,\nNama Menu: Master Jurnal,\nIcon Menu: wb-cloud', '1586930688', '2022-05-25 20:02:51'),
('1653483794431919', 'Menu', 'Update', 'ID Menu: 00001,\nNama Menu: Master,\nIcon Menu: wb-cloud', '1586930688', '2022-05-25 20:03:14'),
('1653483805996189', 'Form', 'Update', 'ID Form: da008,\nNama Form: Master Jurnal,\nID Menu: 00002,\nID Sistem: 008,\nIcon Form: wb-cloud', '1586930688', '2022-05-25 20:03:25'),
('1653483829979015', 'Form', 'Update', 'ID Form: da008,\nNama Form: Master Jurnal,\nID Menu: 00007,\nID Sistem: 008,\nIcon Form: wb-cloud', '1586930688', '2022-05-25 20:03:49'),
('1653484067370561', 'Form', 'Update', 'ID Form: bp008,\nNama Form: Data Unit,\nID Menu: 00007,\nID Sistem: 008,\nIcon Form: wb-align-center', '1586930688', '2022-05-25 20:07:47'),
('1653487888001616', 'Akses', 'Login', 'Login Berhasil', '1629992959', '2022-05-25 21:11:28'),
('1653487920080176', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-25 21:12:00'),
('1653488669089173', 'Menu', 'Tambah', 'ID Menu: 00008,\nNama Menu: Laporan Bumdes,\nIcon Menu: wb-book', '1586930688', '2022-05-25 21:24:29'),
('1653488762719497', 'Form', 'Tambah', 'ID Form: jr008,\nNama Form: admin3,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-bookmark', '1586930688', '2022-05-25 21:26:02'),
('1653488821977518', 'Form', 'Update', 'ID Form: jr008,\nNama Form: Jurnal,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-bookmark', '1586930688', '2022-05-25 21:27:01'),
('1653488910094584', 'Form', 'Hapus', 'ID Form: jr008,\nNama Form: Jurnal,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-bookmark', '1586930688', '2022-05-25 21:28:30'),
('1653488969269634', 'Form', 'Update', 'ID Form: tr008,\nNama Form: Jurnal,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-bookmark', '1586930688', '2022-05-25 21:29:29'),
('1653489089862325', 'Form', 'Tambah', 'ID Form: nr008,\nNama Form: Neraca,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-star-outline', '1586930688', '2022-05-25 21:31:29'),
('1653489164892397', 'Form', 'Tambah', 'ID Form: lr008,\nNama Form: Laba Rugi,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-memory', '1586930688', '2022-05-25 21:32:44'),
('1653489231724288', 'Form', 'Tambah', 'ID Form: bb008,\nNama Form: Buku Besa,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-desktop', '1586930688', '2022-05-25 21:33:51'),
('1653489282969351', 'Form', 'Update', 'ID Form: bb008,\nNama Form: Buku Besar,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-desktop', '1586930688', '2022-05-25 21:34:42'),
('1653489325221756', 'Form', 'Tambah', 'ID Form: lp008,\nNama Form: Laporan,\nID Menu: 00008,\nID Sistem: 008,\nIcon Form: wb-clipboard', '1586930688', '2022-05-25 21:35:25'),
('1653498622940550', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-26 00:10:22'),
('1653498661139859', '008_transaksi', 'Tambah', 'Nama Data Akun: 21102, \nNominal: 10000,\nKeterangan: Nyaor Utang', '1586930688', '2022-05-26 00:11:01'),
('1653498701224526', '008_transaksi', 'Tambah', 'Nama Data Akun: 21101, \nNominal: 10000,\nKeterangan: Utang Bensin', '1586930688', '2022-05-26 00:11:41'),
('1653498748269003', '008_transaksi', 'Update', 'ID: 1630012820,\nUpdate Status Data: nonaktif', '1586930688', '2022-05-26 00:12:28'),
('1653498762637541', '008_transaksi', 'Update', 'ID: 1630012820,\nUpdate Status Data: aktif', '1586930688', '2022-05-26 00:12:42'),
('1653498776402069', '008_transaksi', 'Update', 'ID: 1630012820,\nUpdate Status Data: nonaktif', '1586930688', '2022-05-26 00:12:56'),
('1653499301421063', 'Form Level', 'Update', 'ID: 1613788327,\nID Level: 01,\nID Form: tr008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 0,\nAkses Cetak: 1', '1586930688', '2022-05-26 00:21:41'),
('1653499322339405', 'Akses', 'Login', 'Login Berhasil', '1629992959', '2022-05-26 00:22:02'),
('1653499370829704', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 10000,\nKeterangan: beli bensin', '1629992959', '2022-05-26 00:22:50'),
('1653499463048639', '008_transaksi', 'Tambah', 'Nama Data Akun: 12101, \nNominal: 10000,\nKeterangan: bahan bakar', '1629992959', '2022-05-26 00:24:23'),
('1653500663567895', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-26 00:44:23'),
('1653502771981323', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-26 01:19:31'),
('1653503405620210', 'Akses', 'Login', 'Login Berhasil', '001', '2022-05-26 01:30:05'),
('1653503432210614', 'Akses', 'Login', 'Login Berhasil', '001', '2022-05-26 01:30:32'),
('1653503491971385', 'Akses', 'Login', 'Login Berhasil', '001', '2022-05-26 01:31:31'),
('1653504258805018', 'Akses', 'Login', 'Login Berhasil', '', '2022-05-26 01:44:18'),
('1653504908015248', 'Akses', 'Login', 'Login Berhasil', '001', '2022-05-26 01:55:08'),
('1653505723155799', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-26 02:08:43'),
('1653711839218866', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 11:23:59'),
('1653711936635793', 'Akses', 'Login', 'Login Berhasil', '1629992959', '2022-05-28 11:25:36'),
('1653711963335870', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 11:26:03'),
('1653712123812956', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 11:28:43'),
('1653713070528239', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 11:44:30'),
('1653713120980874', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 11:45:20'),
('1653714097963894', 'Level', 'Update', 'ID Level: 001,\nNama Level: Unit 12', '1586930688', '2022-05-28 12:01:37'),
('1653715449076857', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 12:24:09'),
('1653715965232242', 'Level', 'Update', 'ID Level: 001,\nNama Level: Unit 1', '1586930688', '2022-05-28 12:32:45'),
('1653716417829584', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 12:40:17'),
('1653716549839709', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-05-28 12:42:29'),
('1655238694715242', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-15 03:31:34'),
('1655360540065873', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-16 13:22:20'),
('1655360837928657', 'Level', 'Update', 'ID Level: 001,\nNama Level: Kolam Renang', '1586930688', '2022-06-16 13:27:17'),
('1655361482532642', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-16 13:38:02'),
('1655361592459791', 'Level', 'Tambah', 'ID Level: 002,\nNama Level: Kolam Pancing', '1586930688', '2022-06-16 13:39:52'),
('1655361681595396', 'Level', 'Tambah', 'ID Level: 003,\nNama Level: Jasa Foto Copy', '1586930688', '2022-06-16 13:41:21'),
('1655361724928828', 'Level', 'Tambah', 'ID Level: 004,\nNama Level: Jasa Hostpot Internet', '1586930688', '2022-06-16 13:42:04'),
('1655362992283041', 'Menu', 'Tambah', 'ID Menu: 00009,\nNama Menu: Transaksi,\nIcon Menu: wb-upload', '1586930688', '2022-06-16 14:03:12'),
('1655363090193586', 'Form', 'Tambah', 'ID Form: tm008,\nNama Form: Transaksi Masuk,\nID Menu: 00009,\nID Sistem: 008,\nIcon Form: wb-plus', '1586930688', '2022-06-16 14:04:50'),
('1655363116022148', 'Form', 'Tambah', 'ID Form: tk008,\nNama Form: Transaksi Keluar,\nID Menu: 00009,\nID Sistem: 008,\nIcon Form: wb-minus', '1586930688', '2022-06-16 14:05:16'),
('1655363289806354', 'Akses', 'Tambah', 'Nama Akun: Dicky Wahyu Pratama,\nUsername: dicky,\nPassword: *****,\nID Level: 03,\nStatus: Y', '1586930688', '2022-06-16 14:08:09'),
('1655364977119783', 'Menu', 'Tambah', 'ID Menu: 00010,\nNama Menu: Tambah Akun Unit,\nIcon Menu: wb-users', '1586930688', '2022-06-16 14:36:17'),
('1655365004958061', 'Menu', 'Hapus', 'ID Menu: 00010,\nNama Menu: Tambah Akun Unit,\nIcon Menu: wb-users', '1586930688', '2022-06-16 14:36:44'),
('1655365106585471', 'Form', 'Tambah', 'ID Form: Au008,\nNama Form: Tambah Akun Unit,\nID Menu: 00007,\nID Sistem: 008,\nIcon Form: wb-users', '1586930688', '2022-06-16 14:38:26'),
('1655365739628846', 'Form', 'Update', 'ID Form: Au008,\nNama Form: Tambah Akun Unit,\nID Menu: 00007,\nID Sistem: 008,\nIcon Form: wb-users', '1586930688', '2022-06-16 14:48:59'),
('1655375687739995', '008_akun', 'Tambah', 'ID_akses: ,\nID_unit: ', '1586930688', '2022-06-16 17:34:47'),
('1655376107397730', '008_akun', 'Tambah', 'ID_akses: ,\nID_unit: ', '1586930688', '2022-06-16 17:41:47'),
('1655376141642429', '008_akun', 'Tambah', 'ID_akses: ,\nID_unit: ', '1586930688', '2022-06-16 17:42:21'),
('1655376606848493', '008_akun', 'Tambah', 'ID_akses: 1655363289,\nID_unit: 001', '1586930688', '2022-06-16 17:50:06'),
('1655377675169938', '008_akun', 'Update', 'ID Akun: 1655376606,\nID_akses: 003,\nID_unit: 003', '1586930688', '2022-06-16 18:07:55'),
('1655377695599205', '008_akun', 'Update', 'ID Akun: 1655376606,\nID_akses: 003,\nID_unit: 003', '1586930688', '2022-06-16 18:08:15'),
('1655377831332812', '008_akun', 'Update', 'ID Akun: 1655376606,\nID_akses: 003,\nID_unit: 003', '1586930688', '2022-06-16 18:10:31'),
('1655378005467969', '008_akun', 'Update', 'ID Akun: 1655376606,\nID_akses: 1655363289,\nID_unit: 003', '1586930688', '2022-06-16 18:13:25'),
('1655378016593026', '008_akun', 'Update', 'ID Akun: 1655376606,\nID_akses: 1586930688,\nID_unit: 003', '1586930688', '2022-06-16 18:13:36'),
('1655378029693034', '008_akun', 'Update', 'ID Akun: 1655376606,\nID_akses: 1655363289,\nID_unit: 004', '1586930688', '2022-06-16 18:13:49'),
('1655378950827314', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-16 18:29:10'),
('1655379044972091', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-16 18:30:44'),
('1655379082295291', 'Akses', 'Reset Password', 'ID Akun: 1655363289,\nNama Akun: Dicky Wahyu Pratama,\nUsername: dicky', '1586930688', '2022-06-16 18:31:22'),
('1655379301480620', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-06-16 18:35:01'),
('1655379412822920', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-16 18:36:52'),
('1655381531847235', 'Form Level', 'Tambah', 'ID Level: 03,\nID Form: da008,\nAkses Tambah: 0,\nAkses Update: 0,\nAkses Hapus: 0,\nAkses Cetak: 1', '1586930688', '2022-06-16 19:12:11'),
('1655381558844438', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-06-16 19:12:38'),
('1655536651346620', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-18 14:17:31'),
('1655541456830552', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-06-18 15:37:36'),
('1655541536915305', 'Form Level', 'Update', 'ID: 1655381531,\nID Level: 03,\nID Form: tr008,\nAkses Tambah: 1,\nAkses Update: 0,\nAkses Hapus: 0,\nAkses Cetak: 1', '1586930688', '2022-06-18 15:38:56'),
('1655544570502990', '008_transaksi', 'Tambah', 'Nama Data Akun: 11101, \nNominal: 50000,\nKeterangan: Arisan Unit', '1655363289', '2022-06-18 16:29:30'),
('1655544649951683', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 50000,\nKeterangan: Arisan Unit', '1655363289', '2022-06-18 16:30:49'),
('1655616786804162', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-19 12:33:06'),
('1655616802167202', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-06-19 12:33:22'),
('1655969278756469', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-23 14:27:58'),
('1656172624789989', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-25 22:57:04'),
('1656319929693752', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-27 15:52:09'),
('1656322365736836', '008_transaksi', 'Update', 'ID: 1630011375,\nUpdate Status Data: nonaktif', '1586930688', '2022-06-27 16:32:45'),
('1656322394134358', '008_transaksi', 'Update', 'ID: 1630011375,\nUpdate Status Data: aktif', '1586930688', '2022-06-27 16:33:14'),
('1656322411895067', '008_transaksi', 'Update', 'ID: 1653498701,\nUpdate Status Data: aktif', '1586930688', '2022-06-27 16:33:31'),
('1656332598004571', '008_transaksi', 'Update', 'ID: 1653498701,\nUpdate Status Data: nonaktif', '1586930688', '2022-06-27 19:23:18'),
('1656332603053838', '008_transaksi', 'Update', 'ID: 1653498701,\nUpdate Status Data: nonaktif', '1586930688', '2022-06-27 19:23:23'),
('1656332616732491', '008_transaksi', 'Update', 'ID: 1653498701,\nUpdate Status Data: aktif', '1586930688', '2022-06-27 19:23:36'),
('1656335740185722', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 10000,\nKeterangan: Beli Makan', '1586930688', '2022-06-27 20:15:40'),
('1656336605403310', '008_transaksi', 'Tambah', 'Nama Data Akun: 11101, \nNominal: 20000,\nKeterangan: ngopi', '1586930688', '2022-06-27 20:30:05'),
('1656340979829800', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 10000,\nKeterangan: makan,\nStatus: ', '1586930688', '2022-06-27 21:42:59'),
('1656342667159853', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 12000,\nKeterangan: tambahkan,\nStatus: 11102', '1586930688', '2022-06-27 22:11:07'),
('1656345265640071', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 10000,\nKeterangan: makan,\nstatus: Debit', '1586930688', '2022-06-27 22:54:25'),
('1656345342543962', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 20000,\nKeterangan: makan,\nstatus: Debit', '1586930688', '2022-06-27 22:55:42'),
('1656401117877097', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-06-28 14:25:17'),
('1656735879754534', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-02 11:24:39'),
('1656737244528429', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 30000,\nKeterangan: Tes Kas Keluar,\nstatus: Debit', '1586930688', '2022-07-02 11:47:24'),
('1656737484169024', '008_transaksi', 'Tambah', 'Nama Data Akun: 31101, \nNominal: 30000000,\nKeterangan: Tes Modal Awal', '1586930688', '2022-07-02 11:51:24'),
('1656785230082495', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-03 01:07:10'),
('1656872746924417', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-04 01:25:46'),
('1656872968163658', 'Form Level', 'Update', 'ID: 1655381531,\nID Level: 03,\nID Form: tr008,\nAkses Tambah: 0,\nAkses Update: 0,\nAkses Hapus: 0,\nAkses Cetak: 0', '1586930688', '2022-07-04 01:29:28'),
('1656873062346252', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-07-04 01:31:02'),
('1656911491683595', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-04 12:11:31'),
('1656929724909468', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-04 17:15:24'),
('1657517290853114', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-11 12:28:10'),
('1657765839533078', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-14 09:30:39'),
('1658070467433946', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-17 22:07:47'),
('1658120588539220', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-18 12:03:08'),
('1658123655697959', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 1250000,\nKeterangan: Beli Hosting dan Domain,\nstatus: Debit', '1586930688', '2022-07-18 12:54:15'),
('1658125160530254', 'Level', 'Hapus', 'ID Level: 02,\nNama Level: Supervisor', '1586930688', '2022-07-18 13:19:20'),
('1658818874896611', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-26 14:01:14'),
('1658819612294489', 'Akses', 'Tambah', 'Nama Akun: Admin Unit Kolam Renang,\nUsername: aukr001,\nPassword: *****,\nID Level: 03,\nStatus: Y', '1586930688', '2022-07-26 14:13:32'),
('1658819647881902', 'Akses', 'Tambah', 'Nama Akun: Admin Unit Kolam Pancing,\nUsername: aukp002,\nPassword: *****,\nID Level: 03,\nStatus: Y', '1586930688', '2022-07-26 14:14:07'),
('1658819693870890', 'Akses', 'Tambah', 'Nama Akun: Admin Unit Jasa Foto Copy,\nUsername: aujfc003,\nPassword: *****,\nID Level: 03,\nStatus: Y', '1586930688', '2022-07-26 14:14:53'),
('1658819746106700', 'Akses', 'Tambah', 'Nama Akun: Admin Unit Jasa Hostpot Internet,\nUsername: aujhi004,\nPassword: *****,\nID Level: 03,\nStatus: Y', '1586930688', '2022-07-26 14:15:46'),
('1658819804981766', 'Akses', 'Update', 'ID Akun: 1655363289,\nNama Akun: Admin BUMDes,\nID Level: 01,\nStatus: Y', '1586930688', '2022-07-26 14:16:44'),
('1658819979784472', 'Level', 'Tambah', 'ID Level: 000,\nNama Level: Bumdes PUSAT', '1586930688', '2022-07-26 14:19:39'),
('1658819993055555', '008_akun', 'Update', 'ID Akun: 1655376606,\nID_akses: 1655363289,\nID_unit: 000', '1586930688', '2022-07-26 14:19:53'),
('1658820016045549', '008_akun', 'Tambah', 'ID_akses: 1658819612,\nID_unit: 001', '1586930688', '2022-07-26 14:20:16'),
('1658820031184378', '008_akun', 'Tambah', 'ID_akses: 1658819647,\nID_unit: 002', '1586930688', '2022-07-26 14:20:31'),
('1658820046793469', '008_akun', 'Tambah', 'ID_akses: 1658819693,\nID_unit: 003', '1586930688', '2022-07-26 14:20:46'),
('1658820061215584', '008_akun', 'Tambah', 'ID_akses: 1658819746,\nID_unit: 004', '1586930688', '2022-07-26 14:21:01'),
('1658820553651999', 'Akuntansi', 'Tambah', 'ID Akuntansi: 10000,\nNama Akuntansi: aaaaaaaaaaaaaaaaa,\nKeterangan Akuntansi: aaaaaaaaaaaaaaaaa,\nStatus: debit', '1586930688', '2022-07-26 14:29:13'),
('1659163826665056', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-30 13:50:26'),
('1659237492885897', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-07-31 10:18:12'),
('1659410189526802', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-08-02 10:16:29'),
('1659758029094484', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-08-06 10:53:49'),
('1659927499541301', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-08-08 09:58:19'),
('1660108575636689', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-08-10 12:16:15'),
('1660190986723947', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-08-11 11:09:46'),
('1660622912082165', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-08-16 11:08:32'),
('1661056908625891', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-08-21 11:41:48'),
('1667278928026030', 'Sistem', 'Hapus', 'ID Sistem: 002,\nNama Sistem: Siades,\nDeskripsi: Siades merupakan Sistem Informasi Administrasi Desa yang menangani seputar layanan administrasi pemerintah tingkat desa,\nIcon Sistem: wb-envelope', '1586930688', '2022-11-01 12:02:08'),
('1667278933637467', 'Sistem', 'Hapus', 'ID Sistem: 003,\nNama Sistem: Situba,\nDeskripsi: Situba merupakan Sistem Informasi Stunting Balita yang menangani data terkait pada Balita yang tersebar di berbagai Posyandu Desa,\nIcon Sistem: wb-heart', '1586930688', '2022-11-01 12:02:13'),
('1667278939127854', 'Sistem', 'Hapus', 'ID Sistem: 004,\nNama Sistem: Sipedakin,\nDeskripsi: Sipedakin merupakan Sistem Informasi Pemetaan Data Kemiskinan Penduduk berdasarkan Data Kesejahteraan Sosial,\nIcon Sistem: wb-cloud', '1586930688', '2022-11-01 12:02:19'),
('1667278944046651', 'Sistem', 'Hapus', 'ID Sistem: 005,\nNama Sistem: Simapat,\nDeskripsi: Simapat merupakan Sistem Informasi Manajemen Pajak Terhutang yang menangani Pajak Terhutang Warga di Tingkat Desa,\nIcon Sistem: wb-paperclip', '1586930688', '2022-11-01 12:02:24'),
('1667278948960018', 'Sistem', 'Hapus', 'ID Sistem: 006,\nNama Sistem: Sirpjm,\nDeskripsi: Sirpjm merupakan Sistem Informasi Rencana Pembangunan Jangka Menengah (RPJM) Desa yang menangani Rencana Rembangunan Desa,\nIcon Sistem: wb-home', '1586930688', '2022-11-01 12:02:28'),
('1667278953678992', 'Sistem', 'Hapus', 'ID Sistem: 007,\nNama Sistem: Simonas,\nDeskripsi: Simonas merupakan Sistem Informasi Monitoring Aset Desa yang menangani berbagai invetarisasi Aset yang dimiliki oleh Desa,\nIcon Sistem: wb-extension', '1586930688', '2022-11-01 12:02:33'),
('1667278977719696', 'Sistem', 'Update', 'ID Sistem: 008,\nNama Sistem: Sibumdes,\nDeskripsi: Sibumdes merupakan Sistem Informasi badan usaha milik desa,\nIcon Sistem: wb-shopping-cart', '1586930688', '2022-11-01 12:02:57'),
('1667279483968809', 'Akses', 'Login', 'Login Berhasil', '1658819612', '2022-11-01 12:11:23'),
('1670059251784680', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 16:20:51'),
('1670059327673249', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 16:22:07'),
('1670073016135513', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 20:10:16'),
('1670073877471698', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 20:24:37'),
('1670074386656611', '008_transaksi', 'Tambah', 'Nama Data Akun: 11102, \nNominal: 21000,\nKeterangan: Rokok Surya,\nstatus: Debit', '1586930688', '2022-12-03 20:33:06'),
('1670075046225038', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-12-03 20:44:06'),
('1670075087631909', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 20:44:47'),
('1670075137888025', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: bp008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:45:37'),
('1670075153689269', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: au008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:45:53'),
('1670075176432539', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: bs008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:46:16'),
('1670075190217282', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: da008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:46:30'),
('1670075205964752', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: lp008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:46:45'),
('1670075221461428', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: lr008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:47:01'),
('1670075233595187', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: ns008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:47:13'),
('1670075243530224', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: tk008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:47:23'),
('1670075251802882', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: tm008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:47:31'),
('1670075263385314', 'Form Level', 'Tambah', 'ID Level: 01,\nID Form: tr008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 1,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:47:43'),
('1670075278857682', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-12-03 20:47:58'),
('1670075317358966', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-12-03 20:48:37'),
('1670075327242075', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 20:48:47'),
('1670075354398243', 'Form Level', 'Hapus', 'ID: 1613788327,\nID Level: 01,\nID Form: tr008,\nAkses Tambah: 1,\nAkses Update: 1,\nAkses Hapus: 0,\nAkses Cetak: 1', '1586930688', '2022-12-03 20:49:14'),
('1670075365106878', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2022-12-03 20:49:25'),
('1670077736369001', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 21:28:56'),
('1670078552492292', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 21:42:32'),
('1670078590742820', '008_transaksi', 'Tambah', 'Nama Data Akun: 11101, \nNominal: 200000,\nKeterangan: JP kakek MERAH', '1586930688', '2022-12-03 21:43:10'),
('1670079440035525', '008_transaksi', 'Tambah', 'Nama Data Akun: 12101, \nNominal: 13000,\nKeterangan: Minyak Goreng', '1586930688', '2022-12-03 21:57:20'),
('1670080227127404', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2022-12-03 22:10:27'),
('1673092564740960', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2023-01-07 18:56:04'),
('1673092907070885', '008_transaksi', 'Tambah', 'Nama Data Akun: 51102, \nNominal: 100000,\nKeterangan: Pulsa Token Listrik,\nstatus: Debit', '1586930688', '2023-01-07 19:01:47'),
('1673092951833618', '008_transaksi', 'Tambah', 'Nama Data Akun: 12101, \nNominal: 10000,\nKeterangan: Chip 10K', '1586930688', '2023-01-07 19:02:31'),
('1673093565804450', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2023-01-07 19:12:45'),
('1673098211938899', 'Akses', 'Login', 'Login Berhasil', '1655363289', '2023-01-07 20:30:11'),
('1673106711603077', '008_transaksi', 'Tambah', 'Nama Data Akun: 11201, \nNominal: 20000,\nKeterangan: Masku', '1586930688', '2023-01-07 22:51:51'),
('1673108079144976', '008_transaksi', 'Tambah', 'Nama Data Akun: 11202, \nNominal: 30000,\nKeterangan: Dinanyan,\nstatus: Debit', '1586930688', '2023-01-07 23:14:39'),
('1674670673589240', 'Akses', 'Login', 'Login Berhasil', '1586930688', '2023-01-26 01:17:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `menu`
--

CREATE TABLE `menu` (
  `id` char(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `icon` varchar(25) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `menu`
--

INSERT INTO `menu` (`id`, `nama`, `icon`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('00001', 'Master', 'wb-cloud', '2020-04-15 15:39:27', '2022-05-25 20:03:14', '', '1586930688'),
('00002', 'Data', 'wb-inbox', '2020-04-15 15:39:27', '0000-00-00 00:00:00', '', ''),
('00003', 'Pencarian', 'wb-search', '2020-04-15 15:39:27', '0000-00-00 00:00:00', '', ''),
('00004', 'Laporan', 'wb-print', '2020-04-15 15:39:27', '0000-00-00 00:00:00', '', ''),
('00005', 'Pengaturan', 'wb-settings', '2020-04-15 15:39:27', '0000-00-00 00:00:00', '', ''),
('00006', 'Akses', 'wb-heart', '2021-02-20 06:09:40', '2021-02-20 08:25:02', '1586930688', '1586930688'),
('00007', 'Bumdes', 'wb-list', '2021-03-16 16:29:22', '0000-00-00 00:00:00', '1586930688', ''),
('00008', 'Laporan Bumdes', 'wb-book', '2022-05-25 21:24:29', '0000-00-00 00:00:00', '1586930688', ''),
('00009', 'Transaksi', 'wb-upload', '2022-06-16 14:03:12', '0000-00-00 00:00:00', '1586930688', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sistem`
--

CREATE TABLE `sistem` (
  `id` char(25) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `icon` varchar(25) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_update` datetime NOT NULL,
  `id_buat` char(25) NOT NULL,
  `id_update` char(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sistem`
--

INSERT INTO `sistem` (`id`, `nama`, `deskripsi`, `icon`, `tgl_buat`, `tgl_update`, `id_buat`, `id_update`) VALUES
('001', 'Sidama', 'Sidama merupakan Sistem Informasi Data Master yang memuat berbagai data Induk yang digunakan oleh sistem lain secara Integrasi', 'wb-book', '2020-04-15 15:39:27', '0000-00-00 00:00:00', '', ''),
('008', 'Sibumdes', 'Sibumdes merupakan Sistem Informasi badan usaha milik desa', 'wb-shopping-cart', '2021-03-16 16:25:16', '2022-11-01 12:02:57', '1586930688', '1586930688');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `008_akun`
--
ALTER TABLE `008_akun`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `008_akuntansi1`
--
ALTER TABLE `008_akuntansi1`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `008_kantor`
--
ALTER TABLE `008_kantor`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `008_transaksi1`
--
ALTER TABLE `008_transaksi1`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `akses`
--
ALTER TABLE `akses`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form`
--
ALTER TABLE `form`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `form_level`
--
ALTER TABLE `form_level`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `icon`
--
ALTER TABLE `icon`
  ADD PRIMARY KEY (`nama`);

--
-- Indeks untuk tabel `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `log_history`
--
ALTER TABLE `log_history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `sistem`
--
ALTER TABLE `sistem`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
